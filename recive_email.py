#!/usr/bin/env python2

import poplib
import StringIO
import rfc822
import string
import config

SERVER = "pop.gmail.com"


def get_most_recent_email(username, password, delete_message_afterward=False):
    server = poplib.POP3_SSL(SERVER)
    server.user(username)
    server.pass_(password)

    new_mails = server.stat()[0]
    if new_mails == 0:
        return -1

    email_resp, email_items, email_octets = server.list()
    email_id, email_size = string.split(email_items[-1])
    email_resp, email_text, email_octets = server.retr(email_id)
    email_text = string.join(email_text, "\n")
    email_file = StringIO.StringIO(email_text)
    email_message = rfc822.Message(email_file)

    if delete_message_afterward:
        del_result = server.dele(id)
        print('deleting...')
        print(del_result)
    server.quit()

    return email_message


def read_message_content(msg):
    if msg == -1:
        return 'read_message_content: no new message'
    return msg.fp.read()


def confirm_email(msg):
    if msg['from'] == 'bridges@torproject.org':
        return True
    return False


def get_number_of_new_emails(username, password):
    server = poplib.POP3_SSL(SERVER)
    server.user(username)
    server.pass_(password)
    new_mails = server.stat()[0]
    server.quit()
    return new_mails


def get_bridge(username, password):
    new_mails = get_number_of_new_emails(username, password)
    print('username : ' + username)
    print('new mails : ' + str(new_mails))
    if new_mails == 0:
        print('exited on 0 mails')
        return
    for i in range(new_mails):
        print('checking id : ' + str(i))
        mail = get_most_recent_email(username, password)
        if confirm_email(mail) is False:
            print('unconfirmed email sender. ignoring email.')
            continue
        msg = read_message_content(mail)
        splited_answer = msg.split()
        if splited_answer[15] == 'obfs4':
            print('bridges received!')
            return splited_answer[15:20]


if __name__ == '__main__':
    username = config.USERNAMES[0]
    password = config.PASSWORDS[0]
    get_bridge(username, password)
