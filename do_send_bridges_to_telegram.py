#!/usr/bin/env python2

import os
from config import BOT_TOKEN, CHANNEL_ID
import telepot
import sys
import config
import requests

def paste(text):
    paste_url = 'https://paste.ubuntu.ir'
    data = {'text':text}
    response_to_post_request = requests.post(paste_url, data=data)
    response = response_to_post_request.request
    if(response_to_post_request.status_code == 200):
        return response.url
    return ''

bot = telepot.Bot(BOT_TOKEN)

number_of_files_to_be_sent = 1
try:
    if len(sys.argv) > 1:
        number_of_files_to_be_sent = int(sys.argv[1])
except:
    print('Unknown input argument. exiting...')
    exit(1)

print('trying to send ' + str(number_of_files_to_be_sent) + ' file(s).')

bridges_path = config.PATH_TO_VPN_DIR
available_bridges = os.listdir(bridges_path)
available_bridges.remove('sent')

number_of_available_bridges = len(available_bridges)
print(str(number_of_available_bridges) + ' file(s) available and ready to be sent.')

if len(available_bridges) == 0:
    print('No available bridges found. exiting...')
    exit(0)

if number_of_available_bridges < number_of_files_to_be_sent:
    print('Not enough files are available to be sent. exiting...')
    exit(1)

for file_name_index in range(number_of_files_to_be_sent):
    file_name = available_bridges[file_name_index]
    file_reader = open(bridges_path + file_name, 'r')
    file_content = file_reader.read()
    file_reader.close()
    paste_url = paste(file_content)
    final_content = file_name[0:10] + ' : ' + paste_url + '\n\n' + '`' + file_content + '`'
    bot.sendMessage(CHANNEL_ID, final_content, parse_mode='MarkDown')
    os.rename(bridges_path + file_name, bridges_path + 'sent/' + file_name)
    print(file_name + ' has been marked as sent.')

print('Done!')

