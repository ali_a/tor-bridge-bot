# TeleTor
A bunch of scripts to ease recieving, sending and using tor bridges.
The 3 main scripts are:

1. `do_recive_bridges.py` will send bridge request (obfs4) to `bridges@bridges.torproject.org` with the email accounts that you have provided in `config.py` and waits 60 seconds. Then, it will recive the bridges and save them as a file titled with a timestamp.
2. `do_send_bridges_to_telegram.py` will read files in `PATH_TO_VPN_DIR` and send them to a telegram channel with channel ID of `CHANNEL_ID` via a bot with `BOT_TOKEN` token. All the named variables are placed in `config.py`. After sending them to the channel, the file will be moved to `PATH_TO_VPN_DIR/sent`.
This script accepts 1 integer as argument and it'll indicate how many files we desire to be send to channel.
3. `do_place_bridges.py` will read `PATH_TO_VPN_DIR/sent` and dump the latest bridges that were recived (since the file names are time stamps) and place them to `/etc/tor/torrc` with proper configurations. This script accepts 1 integer as argument and it'll indicate the minimum threshold for bridges to be added. If the latest file doesn't have that many bridges, the program will dump the second latest and so on.

Note: run the scripts with `python2`

Note: Using `do_recive_bridges.py` and then `do_place_bridges.py` will not work as you might expect. `do_place_bridges.py` ought to be run after running `do_place_bridges.py`. I wrote it to fit my needs, you can change it if you want.

Note: rename `config.py.example` to `config.py` and edit it so it will have the needed parameters.

Note: In order to `do_place_bridges.py` work properly, you must grant the user running this script (most likely, it is your own user) the permition to write on `/etc/tor/torrc`. To do that, do `sudo chown root:$USER /etc/tor/torrc` and then `sudo chmod g+w /etc/tor/torrc`

Note: When using an email, there are 2 changes you should make on that email:
- Enable `pop3` and `IMAP` protocols
- enable `less secure devices` in mail services like Gmail so that these scripts can have access to emails.