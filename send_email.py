#!/usr/bin/env python2

import smtplib
import config


def send_email(username, password):
    tor_bridges = 'bridges@bridges.torproject.org'
    text = 'get transport obfs4'

    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(username, password)
    result = server.sendmail(username, tor_bridges, text)
    server.quit()
    return result


if __name__ == '__main__':
    result = send_email(config.USERNAMES[0], config.PASSWORDS[0])
    print(result)

