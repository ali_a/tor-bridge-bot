#!/usr/bin/env python2

"""
sudo chown root:$USER /etc/tor/torrc
sudo chmod g+w /etc/tor/torrc
"""

import os
import sys
import config

bridge_counter = 0
min_bridges = 12

try:
    if len(sys.argv) > 1:
        min_bridges = int(sys.argv[1])
except:
    print('Unknown input argument. exiting...')
    exit(1)

print('Min Bridges threshold : ' + str(min_bridges))

path = config.PATH_TO_VPN_DIR + 'sent/'
dir_content = os.listdir(path)
dir_content.sort()

bridges = ''

while(bridge_counter < min_bridges):
    poped_file_name = dir_content.pop()
    file_content = open(path + poped_file_name, 'r')
    bridges += file_content.read()
    file_content.close()
    bridge_counter = bridges.count('obfs4')
    print(poped_file_name + ' extracted.')
    
print(str(bridge_counter) + ' bridges extracted.')

bridges = bridges.replace('obfs4', 'bridge obfs4')

prefix = 'Log notice file /var/log/tor/notices.log\n\n'
postfix = 'UseBridges 1\nClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy\n'

bridges = prefix + bridges + postfix

tor_file = open('/etc/tor/torrc', 'w')
tor_file.write(bridges)
tor_file.close()

print('Done.')