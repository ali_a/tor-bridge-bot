#!/usr/bin/env python2

import send_email
import recive_email
import config
import telepot
import time

channel_id = config.CHANNEL_ID
bot_token = config.BOT_TOKEN
usernames = config.USERNAMES
passwords = config.PASSWORDS


def do_get_bridge():
    bridges = ''
    return bridges


for email_id in range(len(config.USERNAMES)):
    username = config.USERNAMES[email_id]
    password = config.PASSWORDS[email_id]
    print('sending email via ' + username)
    send_email.send_email(username, password)

for i in range(30, 0, -1):
    print('sleeping. {time} seconds left...'.format(time=i))
    time.sleep(1)

bridges = []
for email_id in range(len(config.USERNAMES)):
    username = config.USERNAMES[email_id]
    password = config.PASSWORDS[email_id]
    ans = recive_email.get_bridge(username, password)
    if ans is None:
        continue
    bridges.extend(ans)

if len(bridges) == 0:
    print('No Bridges Recieved')
    exit(1)

bridges_string = ''
for i in range(int(len(bridges)/5)):
    i = i * 5
    single_bridge = ''
    for j in range(5):
        single_bridge += bridges[i+j] + ' '
    bridges_string += single_bridge + '\n\n'

path = config.PATH_TO_VPN_DIR
file_name = str(time.strftime('%Y-%m-%d-%H-%M-%S'))

save_file = open(path + file_name, 'w')
save_file.write(bridges_string)
save_file.close()
